# Un/Serialize json, reload json if file changes detected

Run the program. When pressing 's' it should create the data.json file inside
data/. Then try making changes to that json file and saving. The program should
detect those changes and update the graphics.
