#pragma once

#include "ofxCereal.h"
#include "ofUtils.h"
#include "ofFileUtils.h"
#include "ofEvent.h"
#include <NotifierBuilder.h>
#include <thread>

class Config {
    public:
        int bgColor {50};
        float circleRadius {20.0f};
        float x {0.5f};
        float y {0.5f};
        // <-- Add properties HERE...

        template<class Archive>
        void serialize(Archive & archive) {
            archive(
                CEREAL_NVP(bgColor),
                CEREAL_NVP(circleRadius),
                CEREAL_NVP(x),
                CEREAL_NVP(y)
                // <-- ...and HERE
            );
        }

        //--------------------------------------------------------------

        Config(std::string p);
        ~Config();

        void load();
        void save();

        ofEvent<bool> onChange;

    private:
        std::thread modifyThread;
        inotify::NotifierBuilder notifier;
        ofFile path;
};

