#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup() {
    // Listen to config file changes
    cfgChangeListener = cfg->onChange.newListener([this](bool &) {
        ofBackground(cfg->bgColor);
    });
    // Load the config file
    cfg->load();

    // Unrelated test for spherical to cartesian
    glm::vec2 angles(1.0f, 0.0f);
    glm::vec3 pos = glm::euclidean(angles);
    std::cout << pos << std::endl;
}

//--------------------------------------------------------------
void ofApp::draw() {
    // Use config value
    ofDrawCircle(ofGetWidth() * cfg->x,
                 ofGetHeight() * cfg->y,
                 cfg->circleRadius);
    foo.draw();
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key) {
    if(key == 's') {
        // Make a change to the config data
        cfg->bgColor = static_cast<int>(ofRandom(255));
        // Save the config file, which triggers a load
        cfg->save();
    }
}
