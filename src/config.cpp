#include "config.h"
#include "ofEventUtils.h"

//--------------------------------------------------------------
Config::Config(std::string p) {
    path = ofFile(ofToDataPath(p, true));

    notifier.watchFile(boost::filesystem::path(path.getEnclosingDirectory()))
    .onEvents({inotify::Event::modify}, [&](inotify::Notification) {
        load();
    });
    modifyThread = std::thread([&]() {
        notifier.run();
    });
}

//--------------------------------------------------------------
Config::~Config() {
    notifier.stop();
    modifyThread.join();
}

//--------------------------------------------------------------
void Config::load() {
    if(ofFile(path).exists()) {
        std::ifstream stream(path.getAbsolutePath());
        cereal::JSONInputArchive archive(stream);
        try {
            archive(CEREAL_NVP(*this));
        } catch (cereal::Exception e) {
            std::cout << e.what() << std::endl;
        }

        bool foo {true};
        ofNotifyEvent(onChange, foo);
    }
}

//--------------------------------------------------------------
void Config::save() {
    std::ofstream stream(path.getAbsolutePath());
    cereal::JSONOutputArchive archive(stream);
    archive(CEREAL_NVP(*this));
}
