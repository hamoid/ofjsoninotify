#include "useconfig.h"
#include "ofAppRunner.h"

UseConfig::UseConfig(std::shared_ptr<Config> cfg) {
    this->cfg = cfg;
}

void UseConfig::draw() {
    float x = ofGetWidth() * cfg->x;
    float y = ofGetHeight() * cfg->y;
    ofDrawLine(x, 0, x, ofGetHeight());
    ofDrawLine(0, y, ofGetWidth(), y);
}
