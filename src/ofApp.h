#pragma once

#include "ofMain.h"
#include "config.h"
#include "useconfig.h"

class ofApp : public ofBaseApp {
    private:

    public:
        // Initialize the shared_ptr cfg
        std::shared_ptr<Config> cfg =
            std::make_shared<Config>("data.json");

        // Optional, to listen for changes in cfg
        ofEventListener cfgChangeListener;

        // An example class that uses cfg
        UseConfig foo {cfg};

        void setup();
        void draw();
        void keyPressed(int key);
};
