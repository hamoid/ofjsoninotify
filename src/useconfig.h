#pragma once

#include "config.h"
#include "ofGraphics.h"

// This is an example of a class that uses Config

class UseConfig {
    private:
        std::shared_ptr<Config> cfg;

    public:
        UseConfig(std::shared_ptr<Config> cfg);
        void draw();
};

